import React from 'react';
import sLogo from "../../imgs/s-logo.svg";
import signoutIcon from "../../imgs/signout-icon.svg";
import signoutHoveredIcon from "../../imgs/signOutHovered.svg";
import dog_logo from "../../imgs/dog_logo.svg";
import zeroSymbol from "../../imgs/zero.svg";
import xSymbol from "../../imgs/x.svg";
import zeroBigSymbol from "../../imgs/xxl-zero.svg";
import xBigSymbol from "../../imgs/xxl-x.svg";
import sendButton from "../../imgs/send-btn.svg";
import girl from "../../imgs/girl.svg";
import boy from "../../imgs/boy.svg";
import block from "../../imgs/block.svg";
import winSymbol from "../../imgs/win-symbol.svg";
import trophy from "../../imgs/trophy.svg";
import close from "../../imgs/close.svg";

const images = {
    xoLogo: sLogo,
    signOut: signoutIcon,
    signOutHovered: signoutHoveredIcon,
    dog: dog_logo,
    miniZero: zeroSymbol,
    bigZero: zeroBigSymbol,
    miniX: xSymbol,
    bigX: xBigSymbol,
    send: sendButton,
    girl: girl,
    boy: boy,
    block: block,
    win: winSymbol,
    trophy: trophy,
    close: close,
};

const MyImage = ({
                    name,
                    width,
                    height,
                    alt,
                    className
                 }) => {
    return (<img className={className} src={images[name]} width={width} height={height} alt={alt}/>);
};

export default MyImage;
