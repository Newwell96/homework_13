import React from 'react';
import './cell.css';
import MyImage from "../../../../../UI components/image/image";

function MyCell(props) {

    return (
        <div className={props.className} onClick={props.onClick}>
            {
                props.value === 'X' ?
                    <MyImage
                        name="bigX"
                        alt="X" />
                    : props.value === 'O' ?
                        <MyImage
                            name="bigZero"
                            alt="O" />
                        : null}
        </div>
    );
}

export default MyCell;