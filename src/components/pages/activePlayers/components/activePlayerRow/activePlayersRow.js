import React, {useState} from 'react';
import Chip from "../../../../UI components/chip/chip";
import MyButton from "../../../../UI components/button/button";
import "./activePlayersRow.css"

const ActivePlayersRow = (props) => {

    const [rowStatus, setRowStatus] = useState(props.status)

    let status = null;
    let chipClassStatus = null;
    let buttonClassStatus = null;

    if (rowStatus) {
        status = "Свободен"
        chipClassStatus = "chip game-status background-light-green"
        buttonClassStatus = "button background-invite-player"
    }
    else {
        status = "В игре"
        chipClassStatus = "chip game-status background-blue"
        buttonClassStatus = "button background-invite-player disable"
    }

    const handleClick = () => {
        setRowStatus(!rowStatus);
        props.onChangeStatus();
    };

    return (
        <tr className="active-players_row">
            <td className="first_colomn">
                {props.name}
            </td>
            <td className="last_column">
                <div
                    className="classChipButton"
                >
                    <Chip
                        className={chipClassStatus}
                        text={status}
                        borderRadius= "6px"
                    />
                    <MyButton
                        onClickFunction={ handleClick }
                        className={buttonClassStatus}
                        buttonText="Позвать играть"
                    />
                </div>
            </td>
        </tr>
    );
};

export default ActivePlayersRow;