import React from 'react';
import "../../../index.css"
import "./components/gameHistoryRow/gameHistoryRow.css"
import gameHistoryData from "./components/data/gameHistory";
import GameHistoryRow from "./components/gameHistoryRow/gameHistoryRow";

const gameHistoryList = gameHistoryData["gameHistory"]

function renderRows (data) {
    return data.map(
        item => (
            <GameHistoryRow
                nameO = {item["nameO"]}
                nameX = {item["nameX"]}
                winner = {item["winner"]}
                date= {item["date"]}
                gameTime = {item["gameTime"]}
            />
        )
    )
}

function GameHistory () {
    return (
        <div className="tables_body">
            <div id="tables_container">
                <h1 className="scores_title">История игр</h1>
                <table
                    className="table games-history"
                >
                    <thead>

                    <tr className="history_row">
                        <th className="players_colomn">
                            Игроки
                        </th>
                        <th className="date_colomn">
                            Дата
                        </th>
                        <th className="gameTimeColomn">
                            Время игры
                        </th>
                    </tr>
                    </thead>

                    <tbody
                        className="games-history"
                    >
                    {renderRows(gameHistoryList)}

                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default GameHistory;
