const users = require("../migrations/db_xoxo_home_public_users.json");
exports.seed = function (knex) {
    // Deletes ALL existing entries
    return knex('users').del()
        .then(function () {
            return knex.insert(users).into('users');
        });
};