// const knex = require("../app.js");
const knexConfig = require('../db/knexfile');
const knex = require('knex')(knexConfig)

const getAccounts = async function (login, password) {

    return await knex('accounts')
        .select("*")
        .where(
            {
                login: login
            }
        )
        .first()
        .then((accounts) => {
            console.log(accounts);
            return accounts;
        })
        .catch((err) => {
            console.error(err);
        })
};

module.exports = getAccounts;