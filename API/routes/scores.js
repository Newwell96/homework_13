var express = require('express');
var router = express.Router();
const authMiddleware = require("../middleware/auth");
const scoresData = require ("../../src/components/pages/scores/data/scoresData");

router.use(authMiddleware)

/* GET home page. */
router.get('/', function (req, res, next) {
    const scoresList = scoresData.scoresList;
    res.status(200);
    res.send({scoresList});
});

module.exports = router;
